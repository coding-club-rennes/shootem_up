/*
** Display.hpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Tue Jan 27 12:07:09 2015 Alexis Bertholom
// Last update Mon Feb 22 22:47:19 2016 baille_l
*/

#ifndef DISPLAY_HPP_
# define DISPLAY_HPP_

# include "types.hpp"

class		Image;

class		Display
{
public:
  virtual ~Display() {}

public:
  virtual void	putPixel(Uint x, Uint y, Color color) = 0;
  virtual void	putRect(Uint x, Uint y, Uint w, Uint h, Color color) = 0;
  virtual void	putImage(Uint x, Uint y, Image& img, Uint w, Uint h) = 0;
  virtual void	putGrid(int tab[4][4], int x, int y) = 0;
  virtual void	clearScreen() = 0;
  virtual void	fillScreen(Color color) = 0;
  virtual void	refreshScreen() = 0;
};

#endif

/*
** Image.hpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Tue Jan 27 12:37:23 2015 Alexis Bertholom
// Last update Tue Nov 21 19:59:33 2017 Lucas
*/

#ifndef IMAGE_HPP_
#define IMAGE_HPP_

#include <SDL2/SDL_ttf.h>
#include <string>
#include <unordered_map>
#include "types.hpp"

struct SDL_Surface;

#define IMAGE_FONT_PATH "assets/"
#define IMAGE_FONT_NAME "Fira.ttf"
#define IMAGE_FONT_SIZE 14

class Image {
 public:
  enum Type { Wrap, Copy };

 public:
  Image(Uint w, Uint h);
  Image(SDL_Surface* img, Image::Type type = Image::Copy);
  ~Image();

 public:
  void putPixel(Uint x, Uint y, Color color);
  void putRect(Uint x, Uint y, Uint w, Uint h, Color color);
  void blit(Uint x, Uint y, Image& img, Uint w, Uint h);
  void clear();
  void fill(Color color);
  void putGrid(int tab[8][8], int x, int y);
  void putText(std::string const& msg, Uint x, Uint y, Color color);
  void putImage(std::string const& texture, Uint x, Uint y);
  SDL_Surface* getSurface();

 public:
  static constexpr Uint max = ~0;

 private:
  SDL_Surface* _img;
  bool _allocd;
  Uint _size;
  std::unordered_map<std::string, SDL_Surface*> _texture_cache;
  TTF_Font* _font_cache;
};

#endif
